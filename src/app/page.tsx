import Cover from '@/components/Cover';
import CardSection from '@/components/sections/CardSection';
import MoreProjects from '@/components/sections/MoreProjects';
import Hero from '@/components/sections/Hero';
import ProjectCardSection from '@/components/sections/ProjectCardSection';
import CompanyProjectCardSection from '@/components/sections/CompanyProjectCardSection';  // Corrected import

import {
 
  moreProjects,
  otherCards,
  projectCard,

  CompanyprojectCard,  // Corrected variable name
} from '@/components/utils/Data';

export default function Home() {
  return (
    <div>
      <Cover />
      <div className="flex flex-col gap-10 lg:px-10">
        {/* <Hero /> */}
        <ProjectCardSection title="Technical Projects" data={projectCard} />
        <MoreProjects title="Practice Projects" data={moreProjects} />

        <CompanyProjectCardSection title="Company Projects" data={CompanyprojectCard} />
     
        <CardSection title="Tools" data={otherCards} />
        <br />
      </div>
    </div>
  );
}
