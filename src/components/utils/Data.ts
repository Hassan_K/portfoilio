import {
  Gamepad2,
  Headphones,
  Image,
  LucideIcon,
  Share2,
  Shirt,
  Coins,
  ShoppingCart,
  Ticket,
} from 'lucide-react';

export interface ISectionCardData {
  id: number;
  title: string;
  src: string;
  progress: number;
  href?: string;
}

export interface IProjectSectionCardData {
  id: number;
  imageUrl?: string;
  icon?: string;
  title: string;
  techStack: string;
  href?: string;
}
export interface CIProjectSectionCardData {
  id: number;
  imageUrl?: string;
  icon?: string;
  title: string;
  techStack: string;
  href?: string;
}

export const projectCard: IProjectSectionCardData[] = [
  {
    id: 1,
    title: 'Travel Next',
    techStack: 'Next.js 14, Typescript, TailwindCSS, Material UI',
    imageUrl: 'travel-next.jpg',
    href: 'https://gitlab.com/Hassan_K/travelnext',
  },
  {
    id: 2,
    title: 'Ecommerce React JS',
    techStack:
      'React JS, Bootstrap, State Management, PayPal checkout SDK',
    imageUrl: 'ecommerce-react.jpg',
    href: 'https://gitlab.com/Hassan_K/ecom-paypal',
  },
  {
    id: 3,
    title: 'VaccineWay',
    techStack:
      'HTML, CSS, JS, PHP, MySQL',
    imageUrl: 'vaccineWay.jpeg',
    href: 'https://gitlab.com/Hassan_K/vaccine-management-system',
  },
  {
    id: 4,
    title: ' WhatsApp Clone',
    techStack: 'React JS, TailwindCSS, Firebase',
    imageUrl: 'whatsapp-clone.png',
    href: 'https://gitlab.com/Hassan_K/whatsapp-clone-react',
  },
];
export const CompanyprojectCard: CIProjectSectionCardData[] = [
{
    id: 1,
    title: 'Leads CRM Admin Dashboard',
    techStack:
      'React JS, Bootstrap, State Management, REST API, Material UI',
    imageUrl: 'crm.png',
    href: '/',
  },
{
    id: 2,
    title: 'Ecommerce React JS',
    techStack:
      'React JS, Bootstrap, State Management, PayPal checkout SDK',
    imageUrl: 'ecommerce-react.jpg',
    href: 'https://gitlab.com/Hassan_K/ecom-paypal',
  },
  {
    id: 3,
    title: 'React Authentication App + Admin Dashboard',
    techStack:
    'React JS, SCSS, REST API, Stripe Clone Api',
    imageUrl: 'admin-dashboard.jpg',
    href: 'https://gitlab.com/Hassan_K/admin_api_react',
  },
 
];

export const moreProjects = [
  {
    id: 1,
    title: 'Expense Tracker',
    techStack: 'React JS',
    icon: 'Coin',
    href: 'https://codesandbox.io/p/sandbox/expense-tracker-htz9y4',
  },
  {
    id: 2,
    title: 'Pagination',
    techStack: 'React JS, dummyjson API',
    icon: 'FileScan',
    href: 'https://codesandbox.io/p/sandbox/react-pagination-dummyjson-hlh9sp',
  },
  {
    id: 3,
    title: 'Investment Calculator',
    techStack: 'React JS',
    icon: 'Calculator',
    href: 'https://codesandbox.io/p/sandbox/investment-prj-forked-n3v6lf',
  },

  {
    id: 4,
    title: 'Timer Game',
    techStack: 'React JS',
    icon: 'Timer',
    href: 'https://codesandbox.io/p/devbox/refs-portals-start-forked-d5tg68?file=%2Fsrc%2FApp.jsx',
  },
];

// export const frontendCard: ISectionCardData[] = [
//   {
//     id: 1,
//     title: 'HTML, CSS',
//     src: '/tech/frontend/htmlcss.jpeg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 2,
//     title: 'Tailwind',
//     src: '/tech/frontend/tailwind.jpg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 3,
//     title: 'Javascript, Typescript',
//     src: '/tech/frontend/jsts.jpeg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 85,
//   },
//   {
//     id: 4,
//     title: 'Reactjs',
//     src: '/tech/frontend/reactjs.jpg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 5,
//     title: 'Nextjs 13',
//     src: '/tech/frontend/nextjs13.jpeg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
// ];

// export const backendCard: ISectionCardData[] = [
//   {
//     id: 1,
//     title: 'NodeJs',
//     src: '/tech/backend/nodejs.webp',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 2,
//     title: 'ExpressJs',
//     src: '/tech/backend/express.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 3,
//     title: 'DenoJs',
//     src: '/tech/backend/denojs.webp',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 60,
//   },
//   {
//     id: 4,
//     title: 'Next Auth',
//     src: '/tech/backend/nextauth.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 5,
//     title: 'PHP',
//     src: '/tech/backend/php.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 70,
//   },
// ];

// export const databaseCard: ISectionCardData[] = [
//   {
//     id: 1,
//     title: 'MySQL',
//     src: '/tech/database/mysql.jpeg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 2,
//     title: 'MongoDB',
//     src: '/tech/database/mongo.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 3,
//     title: 'Redis',
//     src: '/tech/database/redis.jpeg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 70,
//   },
//   {
//     id: 4,
//     title: 'PostgreSQL',
//     src: '/tech/database/postgres.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 70,
//   },
//   {
//     id: 5,
//     title: 'Elastic Search',
//     src: '/tech/database/elasticSearch.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 70,
//   },
// ];

// export const devopsCards: ISectionCardData[] = [
//   {
//     id: 1,
//     title: 'Docker',
//     src: '/tech/devops/docker.webp',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 2,
//     title: 'Kubernetes',
//     src: '/tech/devops/k8s.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 3,
//     title: 'GIT',
//     src: '/tech/devops/git.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 4,
//     title: 'Jenkins',
//     src: '/tech/devops/jenkins.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 70,
//   },
//   {
//     id: 5,
//     title: 'Terraform',
//     src: '/tech/devops/terraform.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 60,
//   },
// ];

// export const web3Cards: ISectionCardData[] = [
//   {
//     id: 1,
//     title: 'Solidity',
//     src: '/tech/web3/solidity.jfif',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 2,
//     title: 'Hardhat',
//     src: '/tech/web3/hardhat.jpeg',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 3,
//     title: 'Metamask',
//     src: '/tech/web3/metamask.webp',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 90,
//   },
//   {
//     id: 4,
//     title: 'Chainlink',
//     src: '/tech/web3/chainlink.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 80,
//   },
//   {
//     id: 5,
//     title: 'OpenZeppelin',
//     src: '/tech/web3/openzeppelin.png',
//     exp: '1 day ago',
//     snippetCount: 1128,
//     progress: 70,
//   },
// ];

export const otherCards: ISectionCardData[] = [
  {
    id: 1,
    title: 'Rest API',
    src: '/projects/rest_api.png',
    progress: 90,
  },
  {
    id: 2,
    title: 'Postman',
    src: '/projects/postman_1.png',
    progress: 70,
  },

];
