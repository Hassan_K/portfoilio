import Link from 'next/link';
import { FC } from 'react';
import Button from '../ui/Button';
import { GitlabIcon } from 'lucide-react';

interface ProfileNameProps {}

const ProfileName: FC<ProfileNameProps> = ({}) => {
    return (
        <div className="text-3xl flex flex-col font-bold text-highlight">
            Hassan Kirmani
            <div className="text-sm font-thin flex justify-between items-center">
              Software Developer
                <Link
                    className="block md:hidden"
                    href="https://gitlab.com/Hassan_K"
                    target='_blank'
                >
                    <Button variant="primary" sizes={'iconOnly'}>
                        <GitlabIcon />
                    </Button>
                </Link>
            </div>
        </div>
    );
};

export default ProfileName;
